import javax.swing.*;

/*
 * 資管4A
 * 105403031  莫智堯
 */

public class Main extends HW7_Frame {
    public static void main(String[] args){
        HW7_Frame hw7_frame = new HW7_Frame();
        hw7_frame.setSize(400, 700);
        hw7_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        hw7_frame.setVisible(true);
    }
}
