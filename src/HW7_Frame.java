import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.regex.PatternSyntaxException;

public class HW7_Frame extends JFrame {
    private JPanel contact_n_addContactPanel = new JPanel();
    private JPanel Q2Panel = new JPanel();
    private JPanel topPanel = new JPanel();
    private JPanel midPanel = new JPanel();
    private JButton searchButton = new JButton("Search");
    private JLabel contactLabel = new JLabel("Contacts");
    private JLabel addContactLabel = new JLabel("+");
    private JTextField searchTextField = new JTextField();
    private JScrollPane scrollPane;
    private int selectRowNumber = 0;
    private int temp1_selectRowNumber;
    private int temp2_selectRowNumber;
    private String selectName = "";
    private Font font1 = new Font("", Font.PLAIN, 30);
    private Font font2 = new Font("", Font.PLAIN, 30);
    private int mouseCount = 0;

    private static final String DATABASE_URL = "jdbc:mysql://localhost/member?autoReconnect=true&useSSL=false";
    private static final String USERNAME = "java";
    private static final String PASSWORD = "java";
    private static final String DEFAULT_QUERY = "SELECT name FROM people";

    private static ResultSetTableModel tableModel;
    private static ResultSetTableModel temp_tableModel;
    private JTable resultTable;
    private JTable temp_resultTable;
    private JFrame newContact;
    private JButton submit_Btn;
    private JComboBox type_comboBox;
    private JTextField name_JTextField;
    private JTextField phone_JTextField;
    private boolean phoneType_matched = false;
    private String[] update_or_delete = {"Delete", "Update"};

    public HW7_Frame(){
        super("Contacts");


        add(Q2Panel);
        Q2Panel.setLayout(new BorderLayout());
        midPanel.setLayout(new BoxLayout(midPanel, BoxLayout.X_AXIS));
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
        contact_n_addContactPanel.setLayout(new BorderLayout());

        midPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        midPanel.add(searchTextField);
        midPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        midPanel.add(searchButton);
        midPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        searchButton.addActionListener(new findButtonHandler());

        searchTextField.addActionListener(new ActionListener() {
            //按下Enter就會自動執行
            public void actionPerformed(ActionEvent e)
            {
                if (searchTextField.getText().length() == 0)
                    allDataDisplay();
                else
                {
                    try
                    {
                        findPerson(searchTextField.getText());
                        findPhone(searchTextField.getText());
                    }
                    catch (PatternSyntaxException pse)
                    {
                        JOptionPane.showMessageDialog(null,
                                "Bad regex pattern", "Bad regex pattern",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }

        });

        addContact();

        contactLabel.setFont(font1);
        addContactLabel.setFont(font1);
        addContactLabel.setForeground(Color.BLUE);
        addContactLabel.setPreferredSize(new Dimension(40,40));
        contact_n_addContactPanel.add(contactLabel, BorderLayout.WEST);
        contact_n_addContactPanel.add(addContactLabel, BorderLayout.EAST);
        topPanel.add(contact_n_addContactPanel);
        topPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        topPanel.add(midPanel);

        Q2Panel.add(topPanel, BorderLayout.NORTH);
        initialDataDisplay();

        Q2Panel.add(scrollPane, BorderLayout.CENTER);

    }

    private void findPhone(String phone) {
        try {
            String FIND_QUERY = "SELECT name FROM people WHERE "
                    + "phone"
                    + " LIKE '%"+phone+"%'";
            tableModel.setQuery(FIND_QUERY);
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }
    }


    private void addContact() {
        addContactLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                newContactPanel();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void newContactPanel() {
        newContact = new JFrame();
        newContact.setSize(400, 700);
        newContact.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        newContact.setVisible(true);
        newContactGUI();
        submit_Btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    newContactCheckphoneType();

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void newContactCheckphoneType() throws SQLException {
        if (type_comboBox.getSelectedItem().toString().matches("company")
                || type_comboBox.getSelectedItem().toString().matches("home")){

            if (phone_JTextField.getText().length() == 8 || phone_JTextField.getText().length() == 9){ //長達8碼/9碼

                char first_digit = phone_JTextField.getText().charAt(0); //取得第一個字元
                if (Character.toString(first_digit).matches("0")){ //第一個字元 == "0"

                    ResultSetTableModel.newContact(name_JTextField.getText(), type_comboBox.getSelectedItem().toString()
                            , phone_JTextField.getText()); //寫進Database

                    JOptionPane.showMessageDialog(null, "成功儲存！！");

                    phoneType_matched = true;
                }
            }

        }else {

            if (phone_JTextField.getText().length() == 10){ //長達10碼

                char first_digit = phone_JTextField.getText().charAt(0); //取得第一個字元
                char second_digit = phone_JTextField.getText().charAt(1); //取得第二個字元

                if (Character.toString(first_digit).matches("0")) { //第一個字元 == "0"
                    if (Character.toString(second_digit).matches("9")){ //第二個字元 == "9"

                        ResultSetTableModel.newContact(name_JTextField.getText(), type_comboBox.getSelectedItem().toString()
                                , phone_JTextField.getText()); //寫進Database

                        JOptionPane.showMessageDialog(null, "成功儲存！！");

                        phoneType_matched = true;
                    }
                }
            }
        }

        if (!phoneType_matched){

            JOptionPane.showMessageDialog(null, "資料建立失敗！\n請檢查電話格式。");

        }

        newContact.setVisible(false);
        allDataDisplay();
    }

    private void newContactGUI() {
        JPanel ContactJPanel_outside = new JPanel();
        JPanel newContact_Panel = new JPanel();
        String[] phone_type = {"company", "home", "cell"};
        JLabel newContactJLabel = new JLabel("New / Edit Contact");
        JLabel name_JLabel = new JLabel("Enter name:");
        JLabel type_JLabel = new JLabel("Select type:");
        JLabel phone_JLabel = new JLabel("Enter phone:");
        name_JTextField = new JTextField();
        type_comboBox = new JComboBox(phone_type);
        phone_JTextField = new JTextField();
        submit_Btn = new JButton("Submit");
        Dimension TextFieldSize = new Dimension(150, 20);

        newContact_Panel.setLayout(new GridLayout(8, 1));

        newContactJLabel.setFont(font1);
        name_JTextField.setPreferredSize(TextFieldSize);
        type_comboBox.setPreferredSize(TextFieldSize);
        phone_JTextField.setPreferredSize(TextFieldSize);
        submit_Btn.setPreferredSize(new Dimension(40, 40));

        newContact_Panel.add(newContactJLabel);
        newContact_Panel.add(name_JLabel);
        newContact_Panel.add(name_JTextField);
        newContact_Panel.add(type_JLabel);
        newContact_Panel.add(type_comboBox);
        newContact_Panel.add(phone_JLabel);
        newContact_Panel.add(phone_JTextField);
        newContact_Panel.add(submit_Btn);

        ContactJPanel_outside.add(newContact_Panel);

        newContact.add(ContactJPanel_outside, BorderLayout.CENTER);
    }

    ////////////////////////////////////////////////
    private void initialDataDisplay() {//初始化顯示
        try {
            tableModel = new ResultSetTableModel(DATABASE_URL, USERNAME, PASSWORD,DEFAULT_QUERY);
            resultTable = new JTable(tableModel);
            resultTable.setFont(font2);
            resultTable.setRowHeight(40);
            scrollPane = new JScrollPane(resultTable);

            final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);//點擊欄位名稱排序
            resultTable.setRowSorter(sorter);
            resultTable.addMouseListener(new mouseAdapter());
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }
    }//end of method initialDataDisplay

    ////////////////////////////////////////////////
    public class mouseAdapter extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {//需要雙擊才會跳出訊息視窗
            mouseCount++;
            if (mouseCount == 1 ) {

                temp1_selectRowNumber = selectRowNumber = resultTable.rowAtPoint(e.getPoint());

            }
            else if (mouseCount==2 ) {

                temp2_selectRowNumber = selectRowNumber = resultTable.rowAtPoint(e.getPoint());

                if(temp1_selectRowNumber==temp2_selectRowNumber) {//點擊同個位置才算雙擊
                    selectName = resultTable.getValueAt(selectRowNumber, 0).toString();
                    selectPeople(selectName);

                    String message = "Type: " + temp_resultTable.getValueAt(0, 2).toString()
                            + "\nPhone no.: " + temp_resultTable.getValueAt(0, 3).toString();

                    int result = JOptionPane.showOptionDialog(null, message, selectName, JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                            null, update_or_delete, "update");

                    if (result == 1){

                        newContactPanel();

                    }else if (result == 0){

                        try {

                            String MemberID = temp_resultTable.getValueAt(0, 0).toString(); //取得MemberID
                            ResultSetTableModel.delete_row(Integer.parseInt(MemberID));

                            allDataDisplay(); //重新整理主畫面


                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }

                    mouseCount=0;
                }

                mouseCount=0;
            }

        }
    }//end of mouseAdapter

    ////////////////////////////////////////////////
    private class findButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            if (searchTextField.getText().length() == 0)//當你甚麼都沒輸入
                allDataDisplay();//顯示全部如同預設
            else
            {
                try
                {
                    findPerson(searchTextField.getText());
                    findPhone(searchTextField.getText());
                }
                catch (PatternSyntaxException pse)
                {
                    JOptionPane.showMessageDialog(null,
                            "Bad regex pattern", "Bad regex pattern",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            mouseCount=0;
        }
    }//end of deleteButtonHandler

    ////////////////////////////////////////////////
    private void allDataDisplay() {
        try {
            tableModel.setQuery(DEFAULT_QUERY);
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }
    }//end of method allDataDisplay

    ////////////////////////////////////////////////
    public void findPerson(String searchText ) {
        try {
            String FIND_QUERY = "SELECT name FROM people WHERE "
                    + "name"
                    + " LIKE '%"+searchTextField.getText()+"%'";
            tableModel.setQuery(FIND_QUERY);
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }

    }//end of method addPerson

    ////////////////////////////////////////////////
    public void selectPeople(String searchText ) {
        try {
            String SELECT_QUERY = "SELECT * FROM people WHERE "
                    + "name"
                    + " LIKE '"+ selectName +"'";
            temp_tableModel = new ResultSetTableModel(DATABASE_URL, USERNAME, PASSWORD, SELECT_QUERY);
            temp_resultTable = new JTable(temp_tableModel);
        }
        catch (SQLException sqlException){
            tableModel.disconnectFromDatabase();
            System.exit(1);
        }

    }//end of method addPerson
}
